import json
from dotenv import dotenv_values
from pycognito import Cognito


# python-HOST=https://lu8wbncagf.execute-api.us-west-2.amazonaws.comdotenv configuration
# config = dotenv_values(".env.dev")  # load shared development variables
config = dotenv_values(".env.qa")  # load shared development variables
# config = dotenv_values(".env.staging")  # load shared development variables


credentials = {
    # 'kinly.qa+11022@gmail.com',  # 'ken+1@1stblvd.com',
    'username': 'kinly.qa+03071@gmail.com',
    'password': 'Morning12',
    'cpx_card_id': 'cd_4quc4ph7htvfd',
}

cpx_headers = {
    'x-api-key': config['CPX_API_KEY'],
    'x-api-version': config['CPX_API_VERSION'],
}

kinly_url = {
    'host': config['HOST'],
    'env_api': config['ENV_API']
}

cpx_host = 'https://cpx-sbx-gl.central-payments.com'


def get_token(user, pw):
    pool_id = config['AWS_USER_POOLS_ID']
    client_id = config['AWS_USER_POOLS_WEB_CLIENT_ID']

    u = Cognito(pool_id, client_id,
                username=user)

    u.authenticate(
        password=pw)

    return u.access_token


def utils_pretify_json(payload):
    json_formatted_str = json.dumps(payload, indent=2)
    return json_formatted_str
