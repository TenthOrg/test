import requests
from utils import utils


def get_card_transactions(card_id):
    ''' Generic GET request to pull card transactions '''
    cpx_host = 'https://cpx-sbx-gl.central-payments.com'
    cpx_endpoint = '/cards/' + card_id + '/transactions'

    r = requests.get(url=cpx_host + cpx_endpoint, headers=utils.cpx_headers)

    status = r.status_code
    headers = r.headers
    payload = r.json()
    pretified_json = utils.utils_pretify_json(payload)

    print(status)
    print(headers)
    print(pretified_json)

    return status, headers, payload
