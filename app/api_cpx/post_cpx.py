import requests
from random import randrange
from utils import utils


def post_cpx_credits(card_id, amount):
    ''' POST request to add credits to card '''

    cpx_endpoint = "/transactions/credits"
    req_url = utils.cpx_host + cpx_endpoint
    req_headers = utils.cpx_headers
    req_payload = {
      "card_id" : card_id,
      "amount" : amount,
      "ext_trans_ref_no" : card_id + "_" + str(randrange(999)),
      "currency_code" : "USD",
      "transaction_type": "CREDIT",
      "merchant" : {
        "mcc" : 7299,
          "mcc_name" : "Retail Corporation",
          "mcc_group" : "Hypermarket",
          "merchant_name" : "WALMART",
          "merchant_city" : "Uniondale",
          "merchant_region" : "New York",
          "merchant_country" : "USA"
      }
    }

    r = requests.post(url=req_url, headers=req_headers,json=req_payload)

    status = r.status_code
    headers = r.headers
    payload = r.json()
    pretified_json = utils.utils_pretify_json(payload)

    print(status)
    print(headers)
    print(pretified_json)

    return status, headers, payload
