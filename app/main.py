
from api_cpx.get_cpx import get_card_transactions
from api_cpx.post_cpx import post_cpx_credits
from api_kinly.get_kinly import *
from api_kinly.post_kinly import *
from api_kinly.get_friends import *

from utils import utils


def kinly_access_token():
    ''' Simple function to retrieve access token  '''

    # Given
    username = utils.credentials['username']
    password = utils.credentials['password']

    # When
    token = utils.get_token(username, password)

    # Then
    assert token
    return token


def kinly_request():
    ''' Simple function to retrieve kinly user info  '''

    # Given
    username = utils.credentials['username']
    password = utils.credentials['password']
    endpoint = '/cognito/user/user-info'

    # When
    response = get_generic_kinly(username, password, endpoint)

    # Then
    assert response
    return response


def friends_request():
    ''' Simple function to retrieve kinly user info  '''

    # Given
    username = utils.credentials['username']
    password = utils.credentials['password']

    # When
    response = get_friends(username, password, endpoint)

    # Then
    assert response
    return response


def cpx_card_transactions(card_id):
    ''' Simple function to retrieve CPX deposit/stash account transactions  '''

    # Given
    card_id = card_id

    # When - Select GET Kinly request, returns status_code,headers,payload
    response = get_card_transactions(card_id)

    # Then - Print
    assert response
    return response


def post_p2p_transfer(from_card_id, to_card_id, amount):
    ''' Simple function to retrieve kinly user info  '''

    # Given
    username = utils.credentials['username']
    password = utils.credentials['password']
    endpoint = '/cognito/user/p2p-transfer'
    payload = {
        "from_card_id": from_card_id,
        "to_card_id": to_card_id,
        "amount": amount
        }

    # When
    response = post_generic_kinly(username, password, endpoint, payload)

    # Then
    assert response
    return response


def main():
    response = "Kinly CLI Tool"

    token = kinly_access_token()

    user_info = kinly_request()
    email = user_info[2]["blvd-user/email"]
    stash_card_id = user_info[2]["blvd-user/cpx-stash"]["cpx-stash/card-id"]
    deposit_card_id = user_info[2]["blvd-user/cpx-deposit"]["cpx-deposit/card-id"]
    last_4 = user_info[2]["blvd-user/cpx-deposit"]["cpx-deposit/last-4"]

    stash_transactions = cpx_card_transactions(stash_card_id)
    deposit_transactions = cpx_card_transactions(deposit_card_id)

    response = post_cpx_credits(deposit_card_id, 199)
    response = post_p2p_transfer(stash_card_id, deposit_card_id, 2)

    # print(email)
    # print(stash_card_id)
    # print(deposit_card_id)
    # print(last_4)
    # print(stash_transactions)
    # print(deposit_transactions)
    print(token)

    return response


if __name__ == '__main__':
    main()
