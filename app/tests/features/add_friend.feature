Feature: PFM-123 - Add Friend
    We are testing all endpoints related to "Add Friend" Feature

    Feature
      https://tenth.atlassian.net/browse/PFM-123

    Desc
      Regardless of utilizing banking or not, Nia is able to be part of a FinLit community to learn and interact with her friends.

    Endpoints
      /friends/get-friends
      /friends/get-sent-pending-invites
      /friends/get-received-pending-invites
      /friends/send-invite
      /friends/accept-invite
      /friends/decline-invite
      /friends/cancel-invite
      /friends/remove-friend

    Endpoint Examples
    GET
      /friends/get-friends
        "user-id":"f14bbf02-8e63-4a24-8b3a-fdca70404e3a"

      /friends/get-sent-pending-invites
        "user-id":"f14bbf02-8e63-4a24-8b3a-fdca70404e3a"

      /friends/get-received-pending-invites
        "user-id":"f14bbf02-8e63-4a24-8b3a-fdca70404e3a"

    POST
      /friends/send-invite
        "to-user-email": "example@example.com"

      /friends/accept-invite
        "invitation-id": "example-id"

      /friends/decline-invite
        "invitation-id": "example-id"

      /friends/cancel-invite
        "invitation-id": "example-id"

    DEL
    /friends/remove-friend
      "user-id":"example-user-id",
      "friend-user-id":"example-user-id"



  Scenario Outline: Get a list of friends for both PFM and Banking user
      Given I am <user>
      When I send <my_request> with <req_payload>
      Then I should see my <expected_result>

    Examples:
      | user        | my_request   | req_payload | expected_result |
      | PFM         | get-friends  | user-id     | correct_list    |
      | Banking     | get-friends  | user-id     | correct_list    |


  Scenario Outline: Get a list of sent pending invites
      Given I am <user>
      When I send <my_request> with <req_payload>
      Then I should see my <expected_result>

    Examples:
      | user        | my_request                | req_payload | expected_result             |
      | PFM         | get-sent-pending-invites  | user-id     | correct_pending_invite_list |
      | Banking     | get-sent-pending-invites  | user-id     | correct_pending_invite_list |


  Scenario Outline: Get a list of received pending invites
      Given I am <user>
      When I send <my_request> with <req_payload>
      Then I should see my <expected_result>

    Examples:
      | user        | my_request                    | req_payload | expected_result              |
      | PFM         | get-received-pending-invites  | user-id     | correct_received_invite_list |
      | Banking     | get-received-pending-invites  | user-id     | correct_received_invite_list |


  Scenario Outline: Send an invite to a friend
      Given I am <user>
      When I send <my_request> with <req_payload>
      Then I should see my <expected_result>

    Examples:
      | user        | my_request  | req_payload   | expected_result        |
      | PFM         | send-invite | to-user-email | successful_invite_send |
      | Banking     | send-invite | to-user-email | successful_invite_send |

  Scenario Outline: Accept an invite from a friend
      Given I am <user>
      When I send <my_request> with <req_payload>
      Then I should see my <expected_result>

    Examples:
      | user        | my_request    | req_payload   | expected_result |
      | PFM         | accept-invite | invitation-id | invite_accepted |
      | Banking     | accept-invite | invitation-id | invite_accepted |
