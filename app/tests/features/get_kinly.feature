Feature: Kinly GET requests
    We are testing Kinly GET requests
    
    Taking this out temporarily for testing in DEV
    | /cognito/user/onboard-non-banking      |


  Scenario Outline: GET user-info API request
      Given I want to get <endpoint>
      When I send a valid request
      Then I should see correct payload

    Examples:
      | endpoint                               |
      | /cognito/user/user-info                |
      | /cognito/user/blvd-info                |
      | /cognito/user/profile-image-upload-url |
      | /cognito/user/direct-deposit-form      |
      | /cognito/user/kyc-attempts             |
      | /cognito/user/accounts-overview-cached |
      | /cognito/user/set-pin-auth-token       |
      | /cognito/user/pin-success              |


  Scenario Outline: GET deposit-card-cached API request
      Given I want to get <endpoint>
      When I send a valid request
      Then I should see correct payload

    Examples:
      | endpoint                               |
      | /cognito/user/deposit-card-cached      |
      | /cognito/user/stash-card-cached        |


  Scenario Outline: Fail GET stash-card-cached API request
      Given I want to get <endpoint>
      When I send an invalid request
      Then I should NOT see correct payload

    Examples:
      | endpoint                               |
      | /cognito/user/deposit-card-cached      |
      | /cognito/user/stash-card-cached        |


  Scenario Outline: GET MX Widget API request
      Given I want to get <endpoint>
      When I send a valid request
      Then I should see correct payload

    Examples:
      | endpoint                               |
      | /cognito/user/mx/connect-widget        |
      | /cognito/user/mx/external-accounts     |
      | /cognito/user/mx/accounts-widget       |
      | /cognito/user/mx/goals-widget          |
      | /cognito/user/mx/budgets-widget        |
