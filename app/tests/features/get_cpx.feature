Feature: CPX GET requests
    We are testing CPX GET requests


  Scenario Outline: I want to pull card transactions
      Given I have this <card>
      When I send a valid request
      Then I should see correct payload

    Examples:
      | card                            |
      | cd_5cmogsdn3rldp                |
      | cd_5cmogupfztvr9                | 
