import pytest
from pytest_bdd import scenario, given, when, then, parsers

from api_kinly.get_friends import get_friends


# --- Scenarios ---
@pytest.mark.skip
@scenario('../features/add_friend.feature',
          'Get a list of friends for both PFM and Banking user')
def test_get_friends():
    ''' GET request to get list of friends '''
    pass


# --- Scenarios ---
@pytest.mark.skip
@scenario('../features/add_friend.feature',
          'Get a list of sent pending invites')
def test_get_sent_pending_invites():
    ''' GET request to get list of send friend invites '''
    pass


# --- Scenarios ---
@pytest.mark.skip
@scenario('../features/add_friend.feature',
          'Get a list of received pending invites')
def test_get_received_pending_invites():
    ''' GET request to get list of received friend invites '''
    pass


# --- Given  ---
@given(parsers.parse("I am {user}"),
       converters=dict(user=str, my_request=str,
                       req_payload=str, expected_result=str),
       target_fixture="context")
def given_steps(user, my_request, req_payload, expected_result):
    # Select user
    if user == "PFM":
        user_id = "289b7349-b01d-4cec-bb4a-7aa2de883e05"  # ken+1@1stblvd.com

    elif user == "Banking":
        user_id = "f14bbf02-8e63-4a24-8b3a-fdca70404e3a"

    # Select my_request
    if my_request == "get-friends":
        pass  # for now...

    # Select req_payload
    if req_payload == "user-id":
        req_payload_internal = {
            "user-id": user_id
        }

    # Select expected_result
    if expected_result == "correct_list":
        expected_res = {
            "friends-info": [],
            "status": "success"
        }
    elif expected_result == "correct_pending_invite_list":
        expected_res = {
            "friends-info": [],
            "status": "success"
        }
    elif expected_result == "correct_received_invite_list":
        expected_res = {
            "friends-info": [],
            "status": "success"
        }
    else:
        xpected_res = {
            "status": "success"
        }

    # Build context fixture for other steps
    context = {
        'username': 'kinly.qa+02011@gmail.com',
        'password': 'Morning12',
        'req_payload': req_payload_internal,
        'my_request': my_request,
        'payload': '',
        'expected_result': expected_res,
        'errors': ''
    }
    return context


# --- When ---
@when(parsers.parse("I send {request}"))
def when_steps(context):
    try:
        # my actual GET request
        result = get_friends(
            context['username'],
            context['password'],
            context['req_payload'],
            context['my_request']
        )  # returns list of status_code, headers, payload

        context['payload'] = result
    except Exception as e:
        context['errors'] = e
        print(e)


# --- Then ---
@then(parsers.parse("I should see my {expected}"))
def then_steps(context):
    assert context['payload'][0] == 200
