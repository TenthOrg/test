from pytest_bdd import scenario, given, when, then, parsers

from api_kinly.get_kinly import get_generic_kinly


# --- Scenarios ---
@scenario('../features/get_kinly.feature', 'GET user-info API request')
def test_user_info():
    ''' GET request to pull user data '''
    pass


@scenario('../features/get_kinly.feature', 'GET deposit-card-cached API request')
def test_get_deposit_card_cached():
    ''' GET request to pull deposit card data '''
    pass


@scenario('../features/get_kinly.feature', 'Fail GET stash-card-cached API request')
def test_get_deposit_card_cached():
    ''' Fail to GET request to pull deposit card data '''
    pass


@scenario('../features/get_kinly.feature', 'GET MX Widget API request')
def test_get_deposit_card_cached():
    ''' GET request to pull MX Widget data '''
    pass

# --- Given  ---


@given(parsers.parse("I want to get {endpoint}"),
       converters={"endpoint": str},
       target_fixture="context")
def given_steps(endpoint):
    # My username and stuff
    context = {
        'username': 'kinly.qa+02011@gmail.com',
        'password': 'Morning12',
        'endpoint': endpoint,  # '/cognito/user/user-info',
        'payload': '',
        'errors': ''
    }
    return context


# --- When ---
@when(parsers.parse("I send a valid {request}"))
def when_steps(context):
    try:
        # my actual GET request
        payload = get_generic_kinly(
            context['username'],
            context['password'],
            context['endpoint']
        )  # returns list of status_code, headers, payload

        context['payload'] = payload
    except Exception as e:
        context['errors'] = e
        print(e)


@when(parsers.parse("I send an invalid {request}"))
def when_steps(context):
    try:
        # my actual GET request
        payload = get_generic_kinly(
            context['username'],
            'Bad_password_data',  # context['password'],
            context['endpoint']
        )  # returns list of status_code, headers, payload

        context['payload'] = payload
    except Exception as e:
        context['errors'] = e
        print(e)


# --- Then ---
@then(parsers.parse("I should see correct payload"))
def then_steps(context):
    assert context['payload'][0] == 200


@then(parsers.parse("I should NOT see correct payload"))
def then_steps(context):
    assert context['errors']  # asserts errors exist
