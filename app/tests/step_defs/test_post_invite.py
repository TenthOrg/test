import pytest


from pytest_bdd import scenario, given, when, then, parsers

from api_kinly.get_friends import get_friends, post_send_invite


# --- Scenarios ---
@pytest.mark.skip
@scenario('../features/add_friend.feature',
          'Send an invite to a friend')
def test_post_send_invite():
    ''' POST send an invite '''
    pass


# --- Given  ---
@given(parsers.parse("I am {user}"),
       converters=dict(user=str, my_request=str,
                       req_payload=str, expected_result=str),
       target_fixture="context")
def given_steps(user, my_request, req_payload, expected_result):
    # Select user
    if user == "PFM":
        user_id = "d7fe5899-5960-4613-a3f2-113a422d34c5"  # ken+2@1stblvd.com
        to_user_email = 'ken+2@1stblvd.com'

    elif user == "Banking":
        user_id = "289b7349-b01d-4cec-bb4a-7aa2de883e05"  # ken+1@1stblvd.com
        to_user_email = 'ken+1@1stblvd.com'

    # Select my_request
    if my_request == "send-invite":
        if user == "PFM":
            to_user_email = 'ken+10132@1stblvd.com'

        elif user == "Banking":
            to_user_email = 'ken+10133@1stblvd.com'

    # Select req_payload
    if req_payload == "to-user-email":

        req_payload_internal = {
            "to-user-email": to_user_email
        }

    # Select expected_result
    if expected_result == "successful_invite_send":
        expected_res = {
            # to-user-email
            "status": "success"
        }
    else:
        expected_res = {
            "status": "success"
        }

    # Build context fixture for other steps
    context = {
        'username': 'kinly.qa+02011@gmail.com',
        'password': 'Morning12',
        'req_payload': req_payload_internal,
        'my_request': my_request,
        'payload': '',
        'expected_result': expected_res,
        'errors': ''
    }
    return context


# --- When ---
@when(parsers.parse("I send {request}"))
def when_steps(context):
    try:
        result = post_send_invite(
            context['username'],
            context['password'],
            context['req_payload'],
            context['my_request']
        )

        context['payload'] = result
    except Exception as e:
        context['errors'] = e
        print(e)


# --- Then ---
@then(parsers.parse("I should see my {expected}"))
def then_steps(context):
    assert context['payload'][0] == 200
