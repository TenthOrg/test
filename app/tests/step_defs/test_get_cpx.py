from pytest_bdd import scenario, given, when, then, parsers

from api_cpx.get_cpx import get_card_transactions


# --- Scenarios ---
@scenario('../features/get_cpx.feature', 'I want to pull card transactions')
def test_user_info():
    ''' GET request to pull card transactions '''
    pass


# --- Given  ---
@given(parsers.parse("I have this {card}"),
       converters={"card": str},
       target_fixture="context")
def given_steps(card):
    # My username and stuff
    print(card)
    context = {
        'card_id': card,
        'payload': '',
        'errors': '',
    }
    return context


# --- When ---
@when(parsers.parse("I send a valid {request}"))
def when_steps(context):
    try:
        # my actual GET request
        payload = get_card_transactions(
            context['card_id'],
        )  # returns list of status_code, headers, payload

        context['payload'] = payload
    except Exception as e:
        context['errors'] = e
        print(e)


# --- Then ---
@then(parsers.parse("I should see correct payload"))
def then_steps(context):
    assert context['payload'][0] == 200
