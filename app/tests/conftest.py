import pytest


@pytest.fixture()
def setup():
    print("Before test run")
    yield
    print("After test run")
