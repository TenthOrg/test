import requests
from utils import utils


def get_friends(username, password, req_payload, my_request):
    ''' GET request to pull friends list '''

    token = utils.get_token(username, password)
    headers = {'Authorization': 'bearer ' + token}
    endpoint = '/friends/' + my_request

    url = utils.kinly_url['host'] + utils.kinly_url['env_api'] + endpoint
    r = requests.get(url=url, headers=headers, data=req_payload)

    status = r.status_code
    headers = r.headers
    payload = r.json()
    pretified_json = utils.utils_pretify_json(payload)

    print(status)
    print(headers)
    print(pretified_json)

    return status, headers, payload


def post_send_invite(username, password, req_payload, my_request):
    ''' POST request to send invite '''

    token = utils.get_token(username, password)
    headers = {'Authorization': 'bearer ' + token}
    endpoint = '/friends/' + my_request

    url = utils.kinly_url['host'] + utils.kinly_url['env_api'] + endpoint
    r = requests.post(url=url, headers=headers, json=req_payload)

    status = r.status_code
    headers = r.headers
    payload = r.json()
    pretified_json = utils.utils_pretify_json(payload)

    print(status)
    print(headers)
    print(pretified_json)

    return status, headers, payload

def post_accept_invite(username, password, req_payload, my_request):
    ''' POST request to accept invite '''
    pass

def post_decline_invite(username, password, req_payload, my_request):
    ''' POST request to decline invite '''
    pass

def post_cancel_invite(username, password, req_payload, my_request):
    ''' POST request to cancel invite '''
    pass

def post_remove_friend(username, password, req_payload, my_request):
    ''' POST request to remove friend '''
    pass
