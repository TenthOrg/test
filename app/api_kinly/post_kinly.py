import requests
from utils import utils


def post_generic_kinly(username, password, endpoint, payload):
    ''' Generic POST request to pull data from endpoints '''

    token = utils.get_token(username, password)
    headers = {'Authorization': 'bearer ' + token}
    url = utils.kinly_url['host'] + utils.kinly_url['env_api'] + endpoint

    r = requests.post(url=url, headers=headers, json=payload)
    pl = r.json()

    return pl
