import requests
from utils import utils


def get_generic_kinly(username, password, endpoint):
    ''' Generic GET request to pull data from endpoints '''

    token = utils.get_token(username, password)
    headers = {'Authorization': 'bearer ' + token}

    url = utils.kinly_url['host'] + utils.kinly_url['env_api'] + endpoint
    r = requests.get(url=url, headers=headers)

    status = r.status_code
    headers = r.headers
    payload = r.json()
    pretified_json = utils.utils_pretify_json(payload)

    print(status)
    print(headers)
    print(pretified_json)

    return status, headers, payload
