from utils import utils


def kinly_access_token():
    ''' Simple function to retrieve access token  '''

    # Given
    username = "kinly.qa+03031@gmail.com"
    password = "Morning12"

    # When
    token = utils.get_token(username, password)

    # Then
    return token


def main():
    token = kinly_access_token()
    print(token)


if __name__ == '__main__':
    main()
