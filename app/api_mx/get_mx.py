import requests
from utils import utils


def get_card_transactions(card_id):
    ''' Generic GET request to pull card transactions '''
    cpx_host = 'https://cpx-sbx-gl.central-payments.com'
    cpx_endpoint = '/cards/' + card_id + '/transactions'

    r = requests.get(url=cpx_host + cpx_endpoint, headers=utils.cpx_headers)
    pl = r.json()

    return pl
