# Test Automation #

This is the setup instructions for the Test Automation scripts. KNS

### What is this repository for? ###

* Quick summary - This test framework uses Pytest, Requests API, and Appium libraries to test the Kinly App.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Python
```sh

# On Mac, install  using Homebrew
brew install python
# Create working directory
mkdir test_automation && chdir test_automation
# install python and check version
python3 --version
# setup python virtual environment
python3 -m venv venv
# activate virtual environment
source venv/bin/activate
# check pip(python's dependency manager like nvm or yarn)
pip list
#


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
